import os
import matplotlib.pyplot as plt
import math

def short(graph, latex = 0):
    if (graph.endswith(".mtx")):
        graph = graph[:-4]
    if (graph == "sd1-arc"):
        graph = "SubDomain"
    if (graph == "out.dbpedia-link"):
        graph = "DBpedia"
    if (graph.startswith("mawi")):
        graph = "mawi"
    if (graph.startswith("Renamed_euk")):
        graph = "eukarya"
    if (graph.startswith("Renamed_arch")):
        graph = "archaea"
    if (latex):
        graph = graph.replace("_", "\\_")
    return graph

sv = {}
cc = {}
stat = {}
files = []
sv_details = {}

for dirName, subdirList, fileList in os.walk("./"):
    for fname in fileList:
        if (fname.find("small.o") != -1):
            files.append(fname)

sorted_cores = []
for fname in files:
    f = file(fname, "r")
    nodes = int(fname.split("_")[1])
    line = f.readline();
    while (line):
        if (line.startswith("Process")):
            ngrid = int(line.strip().split(" ")[9])
            nthreads = int(line.strip().split(" ")[11])
            cores = ngrid * ngrid * nthreads
            line = f.readline()
            cont = line.strip().split(" ")
            graph = cont[1].split("/")[-1]
            line = f.readline()
            sym = False
            error = False
            while (not line.startswith("=")):
                cont = line.strip().split(" ");
                if (line.startswith("Process")):
                    print nthreads
                if (line.startswith("File Read")):
                    load = float(cont[3])
                if (line.startswith("Load")):
                    lb = float(cont[2])
                if (line.startswith("Symmatricizing")):
                    sym = True
                if (line.startswith("As") and not sym):
                    V = int(cont[3])
                    E = int(cont[9])
                if (line.startswith("srun")):
                    error = True
                    break
                line = f.readline()

            if (error):
                continue
            if (line.find("SV") == -1):
                print "something wrong? (SV)"

            sv_total = 0
            sv_iters = 0
            sv_sol = 0

            GetGP = 0.0
            SpMV = 0.0
            Hook = 0.0
            Other = 0.0
            Total = 0.0
            Diff = []
            Spmv = []
            line = f.readline()
            while (not line.startswith("=")):
                cont = line.strip().split(" ");
                if (line.startswith("Total")):
                    sv_total = float(cont[2])
                if (line.startswith("Iteration")):
                    sv_iters += 1
                    Diff.append(int(cont[3][:-1]))
                if (line.startswith("Number")):
                    sv_sol = int(cont[3])
                if (line.startswith("total")):
                    Total += float(cont[1][:-1])
                    GetGP += float(cont[3][:-1])
                    SpMV  += float(cont[5][:-1])
                    Hook  += float(cont[7][:-1])
                    Other += float(cont[9][:-1])
                    Spmv.append(float(cont[5][:-1]))
                line = f.readline()

            line = f.readline()
            line = f.readline()
            if (line.find("LACC") == -1):
                print "something wrong? (LACC)"

            cc_total = 0
            cc_iters = 0
            cc_sol = 0
            cc_converged = 0
            Cond = 0.0
            Uncond = 0.0
            Star = 0.0
            Shortcut = 0.0
            line = f.readline()
            while (not line.startswith("=")):
                cont = line.strip().split(" ");
                if (line.startswith("Total")):
                    cc_total = float(cont[2])
                if (line.startswith("Iteration")):
                    cc_iters = int(cont[1])
                    if (cc_iters == 3):
                        cc_converged = int(cont[3])
                if (line.startswith("Number")):
                    cc_sol = int(cont[3])
                if (line.startswith(" Time:")):
                    Cond   += float(cont[3])
                    Uncond += float(cont[7])
                    Star   += float(cont[5]) + float(cont[9]) + float(cont[13])
                    Shortcut += float(cont[11])
                line = f.readline()

            if (sv_sol != cc_sol):
                print "wrong answer on " + graph
                print "  lacc : " + str(cc_sol)
                print "  sv   : " + str(sv_sol)

            if (short(graph) == "mawi" or short(graph) == "DBpedia"):
                continue
            if (not graph in stat):
                stat[graph] = (V, E, sv_sol, lb, E*1.0/V)
            if (cores == 1024 and not graph in sv_details):
                sv_details[graph] = (Diff, Spmv)
            sorted_cores.append(cores)
            if (not cores in sv):
                sv[cores] = {}
                cc[cores] = {}
            iters = sv_iters
            sv[cores][graph] = ("sv", sv_total, sv_iters, GetGP, SpMV, Hook, Other, Total)
            iters = cc_iters
            cc[cores][graph] = ("lacc", cc_total, cc_iters, Cond, Uncond, Star, Shortcut)
        line = f.readline()
    f.close()

def pretty(num):
    if (num < 1000):
        return str(num);
    if (num < 1000000):
        return "%.2fK" % (num / 1e3)
    if (num < 1000000000):
        return "%.2fM" % (num / 1e6)
    else:
        return "%.2fB" % (num / 1e9)
    return str(num)

def prt_array(a):
    out = ""
    for arr in a:
        if (len(arr) == 0):
            out += "[]"
            continue
        line = "[" + ("%.2f" % arr[0])
        for i in range(1, len(arr)):
            line += ", " + ("%.2f" % arr[i])
        line += "] "
        out += line
    print out
    return

def latex_description():
    sorted_by_size = [(stat[graph][0], graph) for graph in stat]
    sorted_by_size.sort()
    for pair in sorted_by_size:
        graph = pair[1]
        print "%s & %s & %s & %d & \\\\" % (short(graph, 1),
            pretty(stat[graph][0]), pretty(stat[graph][1]), stat[graph][2])

def figure_grid(rows, cols, height=3):
    fig, axarr = plt.subplots(rows, cols, figsize=(cols*4, rows*height))
    if (rows > 1 and cols > 1):
        axlist = []
        for i in range(0, rows):
            for j in range(0, cols):
                axlist.append(axarr[i, j])
    else:
        axlist = axarr
    return fig, axlist

def get_yticks(hi, low):
    ticks = []
    labels = []
    for i in range(0, 12):
        y = 1 << i
        if (i * 2 > low*1.1):
            ticks.append(i)
            labels.append(str(y))
        if (i > 0 and i > hi): break
    return ticks, labels

### summary of the execution time and number of iterations
sorted_cores = list(set(sorted_cores))
sorted_cores.sort()
alldata = []
for cores in sorted_cores:
    print "==================== %d cores ====================" % cores
    print "%7s %7s %6s Exec.(%%) | %7s  %s" % ("SV", "LACC", "Iters", "|E|/|V|", "Graph")
    tosort = []
    for graph in sv[cores]:
        t_sv = sv[cores][graph][1]
        t_cc = cc[cores][graph][1]
        i_sv = sv[cores][graph][2]
        i_cc = cc[cores][graph][2]
        avg_deg = stat[graph][4]
        print "%7.2f %7.2f  %2d/%-2d %7.2f%% | %7.2f  %s" % (t_sv, t_cc, i_sv, i_cc, t_sv*100.0/t_cc, avg_deg, short(graph))
        tosort.append(t_cc / t_sv)
        if (cores > 64):
            alldata.append(t_cc / t_sv)
    tosort.sort()
    avg = sum(tosort) / len(tosort)
    print "avg %.2fx, min %.2fx, max %.2fx" % (avg, tosort[0], tosort[-1])
    print ""
avg = sum(alldata) / len(alldata)
print "averaged speedup for all datasets %.2f" % avg
print ""

## summary of the LAGraph experiment
LAGraph = []
speedup = []
f = file("LAGraph_results.txt", "r")
print "=============== LAGraph =============== "
print "%7s %7s %6s %7s | %s" % ("LACC", "FastSV", "Iters", "Speedup", "Graph")
line = f.readline()
while (line):
    if (line.startswith("reading")):
        graph = line.strip().split(" ")[3]
        g = short(graph)
        line = f.readline() # finish reading
        line = f.readline() # number of iterations
        iter_cc = int(line.strip().split(" ")[3])
        line = f.readline() # number of components
        sol_cc = int(line.strip().split(" ")[3])
        line = f.readline() # time
        if (not line.startswith("LACC")): break
        cont = line.strip().split(" ");
        t_cc = float(cont[1])
        line = f.readline() # number of iterations
        iter_sv = int(line.strip().split(" ")[3])
        line = f.readline() # number of components
        sol_sv = int(line.strip().split(" ")[3])
        line = f.readline() # time
        if (not line.startswith("FastSV")): break
        cont = line.strip().split(" ");
        t_sv = float(cont[1])
        if (sol_cc != sol_sv):
            print "answer mismatch! ", sol_cc, sol_sv
            break
        line = f.readline()
        print "%7.2f %7.2f  %2d/%-2d %5.2fx  | %s" % (t_cc, t_sv, iter_cc, iter_sv, t_cc/t_sv, g)
        LAGraph.append((graph, t_cc, t_sv))
        speedup.append(t_cc/t_sv)
    line = f.readline()
speedup.sort()
avg = sum(speedup) / len(speedup)
print "avg %.2fx, min %.2fx, max %.2fx" % (avg, speedup[0], speedup[-1])
print ""



def density_speedup():
    cores = 1024
    sorted_by_speedup = [(cc[cores][graph][1] / sv[cores][graph][1], stat[graph][4], graph) for graph in sv[cores]]
    sorted_by_speedup.sort()
    speedup = []
    density = []
    for pair in sorted_by_speedup:
        graph = pair[2]
        speedup.append(pair[0])
        density.append(pair[1])
    fig, ax = plt.subplots(1, 1, figsize=(8, 3))
    xs = [i for i in range(0, len(sorted_by_speedup))]
    ax.bar(xs, speedup, width=0.5)
    for i in range(0, len(speedup)):
        s = "%.1fx" % speedup[i]
        ax.text(i, speedup[i]+0.1, s, horizontalalignment='center', fontsize=9)
    ax.set_xticks(xs)
    s_graphs = [short(i[2]) for i in sorted_by_speedup]
    ax.set_xticklabels(s_graphs, rotation=40)
    ax.set_ylabel("Speedup", fontsize=14)
    ax.set_ylim(ymin=0, ymax=5.2)
    #ax.legend(loc="upper left", fontsize=12)
    ax2 = ax.twinx()
    ax2.plot(xs, density, marker="o", markersize=9, linewidth=3, color="red")
    ax2.set_ylabel("Avg. vertex degree", fontsize=14)
    i1 = s_graphs.index("archaea")
    ax2.text(i1 + 0.25, density[i1]+5, "30.1% converged vertices")
    ax2.text(i1 + 0.25, density[i1]-8, "for LACC on iteration 3")
    i2 = s_graphs.index("eukarya")
    ax2.text(i2 + 0.25, density[i2]+5, "32.7% converged vertices")
    ax2.text(i2 + 0.25, density[i2]-8, "for LACC on iteration 3")
    fig.tight_layout()
    fig.savefig("density-speedup.pdf", bbox_inches="tight")

def density_SpMV():
    cores = 4096
    # (density, SpMV, graph)
    sorted_by_density = [(stat[graph][4], sv[cores][graph][4]*100.0/sv[cores][graph][7], graph, stat[graph][0]) for graph in sv[cores]]
    sorted_by_density.sort()
    size = []
    spmv = []
    density = []
    for pair in sorted_by_density:
        graph = pair[2]
        spmv.append(pair[1])
        density.append(pair[0])
        size.append(pair[3])
    fig, ax = plt.subplots(1, 1, figsize=(8, 3))
    xs = [i for i in range(0, len(sorted_by_density))]
    ax.bar(xs, spmv, width=0.5)
    for i in range(0, len(spmv)):
        s = "%.f%%" % (spmv[i])
        ax.text(i, spmv[i]+1, s, horizontalalignment='center', fontsize=9)
    ax.set_xticks(xs)
    s_graphs = [short(i[2]) for i in sorted_by_density]
    ax.set_xticklabels(s_graphs, rotation=40)
    ax.set_ylabel("Time on SpMV (%)", fontsize=14)
    ax.set_ylim(ymin=0, ymax=55)
    #ax.legend(loc="upper left", fontsize=12)
    ax2 = ax.twinx()
    ax2.plot(xs, density, marker="o", markersize=9, linewidth=3, color="red")
    ax2.set_ylabel("Avg. vertex degree", fontsize=14)
    fig.tight_layout()
    fig.savefig("density-spmv.pdf", bbox_inches="tight")

def iterations():
    ### number of iterations
    cores = sorted_cores[-1] # biggest
    sorted_by_size = [(stat[graph][1], graph) for graph in stat]
    #sorted_by_size.sort()
    graphs = [item[1] for item in sorted_by_size]
    y_sv = []
    y_cc = []
    w = 0.3
    for graph in graphs:
        y_cc.append(cc[cores][graph][2])
        y_sv.append(sv[cores][graph][2])
    graphs.extend(["Metaclust50", "Hyperlink"])
    y_cc.extend([9, 9])
    y_sv.extend([9, 10])
    fig, ax = plt.subplots(1, 1, figsize=(8, 3))
    xs = [i-w/2 for i in range(0, len(graphs))]
    ax.bar(xs, y_cc, width=w, label="LACC")
    xs = [i+w/2 for i in range(0, len(graphs))]
    ax.bar(xs, y_sv, width=w, label="FastSV")
    for i in range(0, len(graphs)):
        y = max(y_sv[i], y_cc[i])
        ax.text(i, y+0.1, str(y), horizontalalignment='center')
    xs = [i for i in range(0, len(graphs))]
    ax.set_xticks(xs)
    s_graphs = [short(graph) for graph in graphs]
    ax.set_xticklabels(s_graphs, rotation=40)
    ax.set_ylabel("Number of iterations", fontsize=14)
    ax.set_ylim(ymin=0, ymax=11)
    ax.legend(loc="upper left", fontsize=10)
    fig.savefig("iterations.pdf", bbox_inches="tight")

def breakdown_sv():
    fig, axlist = figure_grid(3, 4)
    x_ticks = [i for i in range(2, len(sorted_cores))]
    x_labels = [str(sorted_cores[i]) for i in x_ticks]

    sorted_ratio = [[], [], []]

    fig_id = 0
    sorted_by_size = [(stat[graph][1], graph) for graph in stat]
    sorted_by_size.sort()
    for pair in sorted_by_size:
        graph = pair[1]
        print short(graph)
        print "%-8s %-8s %-8s %-8s" %("GP", "SpMV", "Hooking", "GP/Hook")
        ys = [[], [], [], []]
        hs = [[], [], [], []]
        for cores in sorted_cores[2:]:
            if (graph in sv[cores]):
                for i in range(0, 4):
                    ys[i].append(sv[cores][graph][i + 3])
            else:
                for i in range(0, 4):
                    ys[i].append(0)
        for j in range(0, len(ys[0])):
            hs[3].append(0)
            for i in [2, 1, 0]:
                hs[i].append(hs[i + 1][j] + ys[i + 1][j])

        xs = [i for i in range(2, len(sorted_cores))]
        ax = axlist[fig_id]
        fig_id += 1
        g = short(graph) + (" (avg. degree %.2f)" % stat[graph][4])
        ax.set_title(g)
        labels = ["GP", "SpMV", "Hooking", "Others"]
        for i in range(0, 4):
            ax.bar(xs, ys[i], 0.5, hs[i], label=labels[i])
        ## output the values
        for j in range(0, len(ys[0])): # different cores
            print "%-8.2f %-8.2f %-8.2f %-8.2f" % (ys[0][j], ys[1][j], ys[2][j], ys[0][j]/ys[2][j])
            sorted_ratio[j].append(ys[0][j]/ys[2][j])
        ax.set_xlim(xmin=x_ticks[0]-0.5, xmax=x_ticks[-1]+0.5)
        ax.set_xticks(x_ticks)
        ax.set_xticklabels(x_labels)
        ax.legend()
        ax.grid()

    for j in range(0, len(ys[0])):
        sorted_ratio[j].sort()
        avg = sum(sorted_ratio[j]) / len(sorted_ratio[j])
        print "average for all datasets %.2f" % avg
        print "min %.2f, max %.2f" % (sorted_ratio[j][0], sorted_ratio[j][-1])

    fig.text(-0.02, 0.5, "Time (sec)", fontsize=18, va="center", rotation="vertical")
    fig.text(0.5, -0.015, "Number of cores", fontsize=18, ha="center")
    fig.tight_layout()
    fig.savefig("breakdown_sv.pdf", bbox_inches="tight")

def breakdown_cc():
    fig, axlist = figure_grid(3, 4)
    x_ticks = [i for i in range(2, len(sorted_cores))]
    x_labels = [str(sorted_cores[i]) for i in x_ticks]

    fig_id = 0
    graphs = [graph for graph in stat]
    for graph in graphs:
        ys = [[], [], [], []]
        hs = [[], [], [], []]
        for cores in sorted_cores[2:]:
            if (graph in cc[cores]):
                for i in range(0, 4):
                    ys[i].append(cc[cores][graph][i + 3] * sv[cores][graph][2])
            else:
                for i in range(0, 4):
                    ys[i].append(0)
        for j in range(0, len(ys[0])):
            hs[3].append(0)
            for i in [2, 1, 0]:
                hs[i].append(hs[i + 1][j] + ys[i + 1][j])

        xs = [i for i in range(2, len(sorted_cores))]
        ax = axlist[fig_id]
        fig_id += 1
        g = short(graph) + (" (avg. degree %.2f)" % stat[graph][4])
        ax.set_title(g)
        labels = ["Conditional hooking", "Unconditional hooking", "Star check", "Shortcutting"]
        for i in range(0, 4):
            ax.bar(xs, ys[i], 0.5, hs[i], label=labels[i])
        ax.set_xlim(xmin=x_ticks[0]-0.5, xmax=x_ticks[-1]+0.5)
        ax.set_xticks(x_ticks)
        ax.set_xticklabels(x_labels)
        ax.legend(fontsize=8)
        ax.grid()
    fig.text(-0.02, 0.5, "Time (sec)", fontsize=18, va="center", rotation="vertical")
    fig.text(0.5, -0.01, "Number of cores", fontsize=18, ha="center")
    fig.tight_layout()
    fig.savefig("breakdown_cc.pdf", bbox_inches="tight")

def scalability(start = 1):
    ### scalability
    fig, axlist = figure_grid(3, 4, height=2.7)
    x_ticks = [i for i in range(start, len(sorted_cores))]
    x_labels = [str(cores) for cores in sorted_cores[start:]]

    fig_id = 0
    sorted_by_size = [(stat[graph][1], graph) for graph in stat]
    sorted_by_size.sort()
    for pair in sorted_by_size:
        graph = pair[1]
        x_both = []
        y_cc = []
        y_sv = []
        index = start
        for cores in sorted_cores[start:]:
            if (graph in sv[cores]):
                x_both.append(index)
                y_cc.append(math.log(cc[cores][graph][1], 2))
                y_sv.append(math.log(sv[cores][graph][1], 2))
            index += 1
        ax = axlist[fig_id]
        fig_id += 1
        g = short(graph)
        ax.set_title(g, fontsize=18)
        ax.plot(x_both, y_cc, label="LACC", marker="o", markersize=9, linewidth=3)
        ax.plot(x_both, y_sv, label="FastSV", marker="s", markersize=9, linewidth=3)
        ax.set_xlim(xmin=start-0.2, xmax=4.2)
        ax.set_xticks(x_ticks)
        ax.set_xticklabels(x_labels, fontsize=15)
        ax.legend(loc="upper right", fontsize=15)
        low = min(y_sv[-1], y_sv[-2])
        y_ticks, y_labels = get_yticks(y_cc[0], low)
        ax.set_yticks(y_ticks)
        ax.set_yticklabels(y_labels, fontsize=15)
        ax.set_ylim(ymin=min(low-0.3, y_ticks[0]), ymax=y_ticks[-1])
        ax.grid()
    fig.text(-0.015, 0.5, "Time in seconds (log scale)", fontsize=22, va="center", rotation="vertical")
    fig.text(0.5, -0.02, "Number of cores (log scale)", fontsize=22, ha="center")
    fig.tight_layout()
    fig.savefig("scalability.pdf", bbox_inches="tight")

def sparsity_spmv():
    fig, axlist = figure_grid(3, 4)
    y_ticks = range(0, 120, 20)
    y_labels = [(str(i)+"%") for i in y_ticks]

    fig_id = 0
    for graph in stat:
        ax = axlist[fig_id]
        fig_id += 1
        if (not graph in sv_details): continue
        iters = len(sv_details[graph][0])
        x_ticks = range(0, iters)
        x_labels = [str(i + 1) for i in x_ticks]
        V = stat[graph][0]
        xs = [i for i in range(0, iters)]
        ys = [100.0]
        for i in sv_details[graph][0][:-1]:
           ys.append(i*100.0/V)
        zs = sv_details[graph][1]
        g = short(graph) + " (density=" + str(stat[graph][4]) + ")"
        ax.set_title(g)
        ax.bar(xs, ys, 0.8)
        ax.set_xlim(xmin=x_ticks[0]-0.5, xmax=x_ticks[-1]+0.5)
        ax.set_xticks(x_ticks)
        ax.set_xticklabels(x_labels)
        ax.set_yticks(y_ticks)
        ax.set_yticklabels(y_labels)
        ax.set_ylim(ymin = 0, ymax = 120)

        ax2 = ax.twinx()
        ax2.plot(xs, zs, marker="o", markersize=9, linewidth=3, color="red")
    fig.text(-0.02, 0.5, "Number of vertices", fontsize=18, va="center", rotation="vertical")
    #fig.text(0.5, -0.01, "Number of cores", fontsize=18, ha="center")
    fig.tight_layout()
    fig.savefig("spmv-all.pdf", bbox_inches="tight")
    fig.clf()

def sparsity_spmv_selected():
    fig, axlist = figure_grid(2, 2)
    y_ticks = range(0, 120, 20)
    y_labels = [(str(i)+"%") for i in y_ticks]

    fig_id = 0
    graphs = [graph for graph in stat]
    selected = [graphs[i] for i in [1,3,6,9]]
    for graph in selected:
        ax = axlist[fig_id]
        fig_id += 1
        if (not graph in sv_details): continue
        iters = len(sv_details[graph][0])
        x_ticks = range(0, iters)
        x_labels = [str(i + 1) for i in x_ticks]
        V = stat[graph][0]
        xs = [i for i in range(0, iters)]
        ys = [100.0]
        for i in sv_details[graph][0][:-1]:
           ys.append(i*100.0/V)
        zs = sv_details[graph][1]
        g = short(graph)
        ax.set_title(g, fontsize=15)
        ax.bar(xs, ys, 0.8)
        ax.set_xlim(xmin=x_ticks[0]-0.5, xmax=x_ticks[-1]+0.5)
        ax.set_xticks(x_ticks)
        ax.set_xticklabels(x_labels)
        ax.set_yticks(y_ticks)
        ax.set_yticklabels(y_labels)
        ax.set_ylim(ymin = 0, ymax = 110)
        ax.set_xlabel("Iteration", fontsize=15)

        ax2 = ax.twinx()
        ax2.plot(xs, zs, marker="o", markersize=9, linewidth=3, color="red")
    fig.text(-0.015, 0.5, "Percentage of vertices used in SpMV", fontsize=18, va="center", rotation="vertical")
    fig.text(1.0, 0.5, "Time for SpMV (Sec)", fontsize=18, va="center", rotation="vertical")
    #fig.text(0.5, -0.01, "Number of cores", fontsize=18, ha="center")
    fig.tight_layout()
    fig.savefig("spmv-selected.pdf", bbox_inches="tight")
    fig.clf()

def breakdown_sv_selected():
    fig, axlist = figure_grid(2, 2)
    graphs = [graph for graph in stat]
    selected = [graphs[i] for i in [1,3,6,9]]

    x_ticks = [i for i in range(2, len(sorted_cores))]
    x_labels = [str(sorted_cores[i]) for i in x_ticks]

    sorted_ratio = [[], [], []]

    fig_id = 0
    for graph in selected:
        #print short(graph)
        #print "%-8s %-8s %-8s %-8s" %("GP", "SpMV", "Hooking", "GP/Hook")
        ys = [[], [], [], []]
        hs = [[], [], [], []]
        for cores in sorted_cores[2:]:
            if (graph in sv[cores]):
                for i in range(0, 4):
                    ys[i].append(sv[cores][graph][i + 3])
            else:
                for i in range(0, 4):
                    ys[i].append(0)
        for j in range(0, len(ys[0])):
            hs[3].append(0)
            for i in [2, 1, 0]:
                hs[i].append(hs[i + 1][j] + ys[i + 1][j])

        xs = [i for i in range(2, len(sorted_cores))]
        ax = axlist[fig_id]
        fig_id += 1
        g = short(graph) + (" (density %.2f)" % stat[graph][4])
        ax.set_title(g, fontsize=14)
        labels = ["Grandparent", "SpMV", "Hooking", "Termination"]
        for i in range(0, 3):
            ax.bar(xs, ys[i], 0.5, hs[i], label=labels[i], zorder=3)
        ## output the values
        for j in range(0, len(ys[0])): # different cores
            #print "%-8.2f %-8.2f %-8.2f %-8.2f" % (ys[0][j], ys[1][j], ys[2][j], ys[0][j]/ys[2][j])
            sorted_ratio[j].append(ys[0][j]/ys[2][j])
        ax.set_xlim(xmin=x_ticks[0]-0.5, xmax=x_ticks[-1]+0.5)
        ax.set_xticks(x_ticks)
        ax.set_xticklabels(x_labels, fontsize=12)
        if (short(graph) == "SubDomain"):
            y_ticks = [3, 6, 9, 12, 15, 18]
            y_labels = [str(i) for i in y_ticks]
            ax.set_yticks(y_ticks)
            ax.set_yticklabels(y_labels)
        ax.legend(fontsize=14)
        ax.yaxis.grid(True, zorder=0)

    for j in range(0, len(ys[0])):
        sorted_ratio[j].sort()
        avg = sum(sorted_ratio[j]) / len(sorted_ratio[j])
        print "average for all datasets %.2f" % avg
        print "min %.2f, max %.2f" % (sorted_ratio[j][0], sorted_ratio[j][-1])

    fig.text(-0.02, 0.5, "Time (sec)", fontsize=18, va="center", rotation="vertical")
    fig.text(0.5, -0.01, "Number of cores", fontsize=18, ha="center")
    fig.tight_layout()
    fig.savefig("breakdown_sv_selected.pdf", bbox_inches="tight")
    fig.clf()

def sv_iters():
    data = [
        ("Queen_4147",10, 10, 10,  9,  8),
        ("kmer_A2a",  15, 13, 12, 12, 11),
        ("archaea",   11, 10,  9,  8,  7),
        ("kmer_V1r",  15, 12, 11, 11, 10),
        ("HV15R",     10,  9,  9,  8,  7),
        ("uk-2002",   13, 11,  9,  8,  7),
        ("eukarya",   11, 10,  9,  8,  7),
        ("uk-2005",   11,  9,  7,  7,  6),
        ("twitter7",   7,  7,  6,  6,  5),
        ("SubDomain", 11, 10,  8,  8,  7),
        ("sk-2005",   11,  9,  7,  7,  6),
    ]

    cc_map = LACC_iters()
    cc_arr = []
    sv_arr = [[], [], [], [], []]
    s_graphs = [item[0] for item in data]
    speedup = []
    for i in range(0, len(data)):
        for j in range(0, 5):
            sv_arr[j].append(data[i][j + 1])
        cc_arr.append(cc_map[data[i][0]])
        speedup.append(sv_arr[0][i] * 100.0 / cc_arr[i] - 100)

    w = 0.13
    labels = [
        "sv1 (simplified SV)",
        "sv2 (sv1 + hook to grandparent)",
        "sv3 (sv2 + stochastic hooking)",
        "sv4 (sv3 + aggressive hooking)",
        "sv5 (sv4 + early termination)"]
    fig, ax = plt.subplots(1, 1, figsize=(14, 3))
    for j in range(0, 5):
        xs = [i+(j-2.5)*w for i in range(0, len(s_graphs))]
        ys = sv_arr[j]
        ax.bar(xs, ys, width=w*0.95, label=labels[j])
    xs = [i+2.5*w for i in range(0, len(s_graphs))]
    ax.bar(xs, cc_arr, width=w*0.95, label="LACC")
    xs = [i for i in range(0, len(s_graphs))]
    ax.set_xticks(xs)
    ax.set_xticklabels(s_graphs, fontsize=11)
    ax.set_ylabel("Number of iterations", fontsize=14)
    ax.set_ylim(ymin=0, ymax=16)
    ax.set_xlim(xmin=-0.6, xmax=10.6)
    ax.legend(loc="upper right", fontsize=12, bbox_to_anchor=(1.33, 1.0))
    fig.savefig("sv_iters.pdf", bbox_inches="tight")

    speedup.sort()
    avg = sum(speedup) / len(speedup)
    print "avg. %.2f%%, min %.2f%%, max %.2f%%" % (avg, speedup[0], speedup[-1])

def lagraph_time():
    arr = [[], []]
    s_graphs = [short(item[0]) for item in LAGraph]
    for i in range(0, len(LAGraph)):
        for j in range(0, 2):
            arr[j].append(LAGraph[i][j + 1])

    w = 0.4
    labels = ["LACC", "FastSV"]
    fig, ax = plt.subplots(1, 1, figsize=(8, 2.5))
    for j in range(0, 2):
        xs = [i+(j-0.5)*w for i in range(0, len(s_graphs))]
        ys = arr[j]
        ax.bar(xs, ys, width=w*0.95, label=labels[j])
        for i in range(0, len(ys)):
            s = "%.2f" % ys[i]
            ax.text(xs[i], ys[i]+1, s, horizontalalignment='center', fontsize=9)
    xs = [i for i in range(0, len(s_graphs))]
    ax.set_xticks(xs)
    ax.set_xticklabels(s_graphs, fontsize=11)
    ax.set_ylabel("Time (sec)", fontsize=14)
    ax.set_ylim(ymin=0, ymax=65)
    #ax.set_xlim(xmin=-0.6, xmax=10.6)
    ax.legend(loc="upper left", fontsize=12)
    fig.savefig("LAGraph.pdf", bbox_inches="tight")

def LACC_iters():
    fname = "LACC_results.txt"
    ret = {}
    f = file(fname, "r")
    line = f.readline()
    while (line):
        if (line.startswith("#")):
            graph = line.strip().split(" ")[1]
            line = f.readline()
            cc_iters = 0
            while (not line.startswith("=")):
                if (line.startswith("Iteration")):
                    cont = line.strip().split(" ");
                    cc_iters = int(cont[1])
                line = f.readline()
            ret[graph] = cc_iters
        line = f.readline()
    f.close()
    return ret

#latex_description()
#iterations()
#density_speedup()
#density_SpMV()
#scalability()
#breakdown_sv()
#breakdown_cc()
#sparsity_spmv()
#sparsity_spmv_selected()
sv_iters()
#breakdown_sv_selected()
#lagraph_time()