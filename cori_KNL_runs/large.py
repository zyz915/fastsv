import os
import matplotlib.pyplot as plt
import math

sv = {}
cc = {}
stat = {}
files = []

for dirName, subdirList, fileList in os.walk("./"):
    for fname in fileList:
        if (fname.find("large.o") != -1):
            files.append(fname)

sorted_cores = []
for fname in files:
    f = file(fname, "r")
    nodes = int(fname.split("_")[1])
    line = f.readline();
    error = False
    while (line):
        if (line.startswith("Process")):
            ngrid = int(line.strip().split(" ")[9])
            nthreads = int(line.strip().split(" ")[11])
            cores = ngrid * ngrid * nthreads
            line = f.readline()
            cont = line.strip().split(" ")
            graph = cont[1].split("/")[-1]
            line = f.readline()
            sym = False
            while (not line.startswith("=")):
                cont = line.strip().split(" ");
                if (line.startswith("Process")):
                    print nthreads
                if (line.startswith("File Read")):
                    load = float(cont[3])
                if (line.startswith("Load")):
                    lb = float(cont[2])
                if (line.startswith("Symmatricizing")):
                    sym = True
                if (line.startswith("As") and not sym):
                    V = int(cont[3])
                    E = int(cont[9])
                if (line.startswith("srun")):
                    error = True
                    break
                line = f.readline()

            if (error):
                line = f.readline()
                continue
            if (line.find("SV") == -1):
                print "something wrong? (SV)"
                continue

            sv_total = 0
            sv_iters = 0
            sv_sol = 0

            GetGP = 0.0
            SpMV = 0.0
            Hook = 0.0
            Other = 0.0
            line = f.readline()
            while (not line.startswith("=")):
                cont = line.strip().split(" ");
                if (line.startswith("Total")):
                    sv_total = float(cont[2])
                if (line.startswith("Iteration")):
                    sv_iters += 1
                if (line.startswith("Number")):
                    sv_sol = int(cont[3])
                if (line.startswith("total")):
                    GetGP += float(cont[3][:-1])
                    SpMV  += float(cont[5][:-1])
                    Hook  += float(cont[7][:-1])
                    Other += float(cont[9][:-1])
                line = f.readline()

            if (not cores in sv):
                sv[cores] = {}
            sv[cores][graph] = ("sv", sv_total, sv_iters, GetGP, SpMV, Hook, Other)

            line = f.readline()
            line = f.readline()
            if (line.find("LACC") == -1):
                print "something wrong? (LACC)"
                continue

            cc_total = 0
            cc_iters = 0
            cc_sol = 0
            cc_converged = 0
            Cond = 0.0
            Uncond = 0.0
            Star = 0.0
            Shortcut = 0.0
            line = f.readline()
            while (not line.startswith("=")):
                cont = line.strip().split(" ");
                if (line.startswith("Total")):
                    cc_total = float(cont[2])
                if (line.startswith("Iteration")):
                    cc_iters = int(cont[1])
                    if (cc_iters == 3):
                        cc_converged = int(cont[3])
                if (line.startswith("Number")):
                    cc_sol = int(cont[3])
                if (line.startswith(" Time:")):
                    Cond   += float(cont[3])
                    Uncond += float(cont[7])
                    Star   += float(cont[5]) + float(cont[9]) + float(cont[13])
                    Shortcut += float(cont[11])
                line = f.readline()

            if (sv_sol != cc_sol):
                print "wrong answer on " + graph
                print "  lacc : " + str(cc_sol)
                print "  sv   : " + str(sv_sol)

            if (not graph in stat):
                stat[graph] = (V, E, sv_sol, lb, E/V)

            sorted_cores.append(cores)
            if (not cores in cc):
                cc[cores] = {}
            cc[cores][graph] = ("lacc", cc_total, cc_iters, Cond, Uncond, Star, Shortcut)
        line = f.readline()
    f.close()

def pretty(num):
    if (num < 1000):
        return str(num);
    if (num < 1000000):
        return "%.2fK" % (num / 1e3)
    if (num < 1000000000):
        return "%.2fM" % (num / 1e6)
    else:
        return "%.2fB" % (num / 1e9)
    return str(num)

def prt_array(a):
    out = ""
    for arr in a:
        if (len(arr) == 0):
            out += "[]"
            continue
        line = "[" + ("%.2f" % arr[0])
        for i in range(1, len(arr)):
            line += ", " + ("%.2f" % arr[i])
        line += "] "
        out += line
    print out
    return

def short(graph, latex = 0):
    if (graph.endswith(".mtx")):
        graph = graph[:-4]
    if (graph == "sd1-arc"):
        graph = "SubDomain"
    if (graph == "out.dbpedia-link"):
        graph = "DBpedia"
    if (graph.startswith("mawi")):
        graph = "mawi"
    if (graph.startswith("Renamed_euk")):
        graph = "eukarya"
    if (graph.startswith("Renamed_arch")):
        graph = "archaea"
    if (graph.startswith("Renamed_graph_Metaclust50")):
        graph = "Metaclust50"
    if (graph.startswith("all_hyperlinks")):
        graph = "Hyperlink"
    if (latex):
        graph = graph.replace("_", "\\_")
    return graph

def latex_description():
    sorted_by_size = [(stat[graph][0], graph) for graph in stat]
    sorted_by_size.sort()
    for pair in sorted_by_size:
        graph = pair[1]
        print "%s & %s & %s & %d & \\\\" % (short(graph, 1),
            pretty(stat[graph][0]), pretty(stat[graph][1]), stat[graph][2])

def get_yticks(hi, low):
    ticks = []
    labels = []
    for i in range(0, 12):
        y = 1 << i
        if (i * 2 > low*1.1):
            ticks.append(i)
            labels.append(str(y))
        if (i > 0 and i > hi): break
    return ticks, labels

### summary of the execution time and number of iterations
sorted_cores = list(set(sorted_cores))
sorted_cores.sort()
for cores in sorted_cores:
    print "==================== %d cores ====================" % cores
    print "%7s %7s %6s Exec.(%%) | %7s %s" % ("SV", "LACC", "Iters", "|E|/|V|", "Graph")
    tosort = []
    for graph in sv[cores]:
        if (graph in cc[cores]):
            t_sv = sv[cores][graph][1]
            t_cc = cc[cores][graph][1]
            i_sv = sv[cores][graph][2]
            i_cc = cc[cores][graph][2]
            avg_deg = stat[graph][4]
            print "%7.2f %7.2f  %2d/%-2d %7.2f%% | %7.2f %s" % (t_sv, t_cc, i_sv, i_cc, t_sv*100.0/t_cc, avg_deg, short(graph))
            tosort.append(t_cc / t_sv)
    tosort.sort()
    avg = sum(tosort) / len(tosort)
    print "avg %.2fx, min %.2fx, max %.2f" % (avg, tosort[0], tosort[-1])
    print ""

def density():
    cores = 1024
    sorted_by_speedup = [(cc[cores][graph][1] / sv[cores][graph][1], stat[graph][4], graph) for graph in sv[cores]]
    sorted_by_speedup.sort()
    speedup = []
    density = []
    for pair in sorted_by_speedup:
        graph = pair[2]
        speedup.append(pair[0])
        density.append(pair[1])
    fig, ax = plt.subplots(1, 1, figsize=(8, 3))
    xs = [i for i in range(0, len(sorted_by_speedup))]
    ax.bar(xs, speedup, width=0.5)
    for i in range(0, len(speedup)):
        s = "%.1fx" % speedup[i]
        ax.text(i, speedup[i]+0.1, s, horizontalalignment='center', fontsize=9)
    ax.set_xticks(xs)
    s_graphs = [short(i[2]) for i in sorted_by_speedup]
    ax.set_xticklabels(s_graphs, rotation=40)
    ax.set_ylabel("Speedup", fontsize=14)
    ax.set_ylim(ymin=0, ymax=5.2)
    #ax.legend(loc="upper left", fontsize=12)
    ax2 = ax.twinx()
    ax2.plot(xs, density, marker="o", markersize=9, linewidth=3, color="red")
    ax2.set_ylabel("Avg. vertex degree", fontsize=14)
    ax2.text(1.2, density[1]+5, "30.1% converged vertices")
    ax2.text(1.2, density[1]-8, "for LACC on iteration 3")
    ax2.text(6.2, density[6]+5, "32.7% converged vertices")
    ax2.text(6.2, density[6]-8, "for LACC on iteration 3")
    fig.tight_layout()
    fig.savefig("density-speedup.pdf", bbox_inches="tight")

def iters():
    ### number of iterations
    cores = sorted_cores[-1] # biggest
    graphs = [graph for graph in sv[cores]]
    y_sv = []
    y_cc = []
    w = 0.3
    for graph in graphs:
        y_cc.append(cc[cores][graph][2])
        y_sv.append(sv[cores][graph][2])
    fig, ax = plt.subplots(1, 1, figsize=(8, 3))
    xs = [i-w/2 for i in range(0, len(graphs))]
    ax.bar(xs, y_cc, width=w, label="LACC")
    xs = [i+w/2 for i in range(0, len(graphs))]
    ax.bar(xs, y_sv, width=w, label="SV")
    for i in range(0, len(graphs)):
        y = max(y_sv[i], y_cc[i])
        ax.text(i, y+0.1, str(y), horizontalalignment='center')
    xs = [i for i in range(0, len(graphs))]
    ax.set_xticks(xs)
    s_graphs = [short(graph) for graph in graphs]
    ax.set_xticklabels(s_graphs, rotation=40)
    ax.set_ylabel("Number of iterations", fontsize=14)
    ax.set_ylim(ymin=0, ymax=11)
    ax.legend(loc="upper left", fontsize=10)
    fig.savefig("iterations.pdf", bbox_inches="tight")

def breakdown_sv():
    grid = (4, 4)
    fig, axarr = plt.subplots(grid[0], grid[1], figsize=(grid[0]*4, grid[1]*3))
    axlist = []
    for i in range(0, grid[0]):
        for j in range(0, grid[1]):
            axlist.append(axarr[i, j])

    x_ticks = [i for i in range(2, len(sorted_cores))]
    x_labels = [str(sorted_cores[i]) for i in x_ticks]

    fig_id = 0
    graphs = [graph for graph in stat]
    for graph in graphs:
        ys = [[], [], [], []]
        hs = [[], [], [], []]
        for cores in sorted_cores[2:]:
            if (graph in sv[cores]):
                for i in range(0, 4):
                    ys[i].append(sv[cores][graph][i + 3])
            else:
                for i in range(0, 4):
                    ys[i].append(0)
        for j in range(0, len(ys[0])):
            hs[3].append(0)
            for i in [2, 1, 0]:
                hs[i].append(hs[i + 1][j] + ys[i + 1][j])

        xs = [i for i in range(2, len(sorted_cores))]
        ax = axlist[fig_id]
        fig_id += 1
        g = short(graph) + (" (avg. degree %.2f)" % stat[graph][4])
        ax.set_title(g)
        labels = ["GP", "SpMV", "Hooking", "Others"]
        for i in range(0, 4):
            ax.bar(xs, ys[i], 0.5, hs[i], label=labels[i])
        ax.set_xlim(xmin=x_ticks[0]-0.5, xmax=x_ticks[-1]+0.5)
        ax.set_xticks(x_ticks)
        ax.set_xticklabels(x_labels)
        ax.legend()
        ax.grid()
    fig.text(-0.02, 0.5, "Time (sec)", fontsize=18, va="center", rotation="vertical")
    fig.text(0.5, -0.01, "Number of cores", fontsize=18, ha="center")
    fig.tight_layout()
    fig.savefig("breakdown_sv.pdf", bbox_inches="tight")

def breakdown_cc():
    grid = (4, 4)
    fig, axarr = plt.subplots(grid[0], grid[1], figsize=(grid[0]*4, grid[1]*3))
    axlist = []
    for i in range(0, grid[0]):
        for j in range(0, grid[1]):
            axlist.append(axarr[i, j])

    x_ticks = [i for i in range(2, len(sorted_cores))]
    x_labels = [str(sorted_cores[i]) for i in x_ticks]

    fig_id = 0
    graphs = [graph for graph in stat]
    for graph in graphs:
        ys = [[], [], [], []]
        hs = [[], [], [], []]
        for cores in sorted_cores[2:]:
            if (graph in cc[cores]):
                for i in range(0, 4):
                    ys[i].append(cc[cores][graph][i + 3] * cc[cores][graph][2])
            else:
                for i in range(0, 4):
                    ys[i].append(0)
        for j in range(0, len(ys[0])):
            hs[3].append(0)
            for i in [2, 1, 0]:
                hs[i].append(hs[i + 1][j] + ys[i + 1][j])

        xs = [i for i in range(2, len(sorted_cores))]
        ax = axlist[fig_id]
        fig_id += 1
        g = short(graph) + (" (avg. degree %.2f)" % stat[graph][4])
        ax.set_title(g)
        labels = ["Conditional hooking", "Unconditional hooking", "Star check", "Shortcutting"]
        for i in range(0, 4):
            ax.bar(xs, ys[i], 0.5, hs[i], label=labels[i])
        ax.set_xlim(xmin=x_ticks[0]-0.5, xmax=x_ticks[-1]+0.5)
        ax.set_xticks(x_ticks)
        ax.set_xticklabels(x_labels)
        ax.legend(fontsize=8)
        ax.grid()
    fig.text(-0.02, 0.5, "Time (sec)", fontsize=18, va="center", rotation="vertical")
    fig.text(0.5, -0.01, "Number of cores", fontsize=18, ha="center")
    fig.tight_layout()
    fig.savefig("breakdown_cc.pdf", bbox_inches="tight")

def large_graph():
    ### scalability
    x_ticks = [i for i in range(0, len(sorted_cores))]
    x_labels = [str(cores) for cores in sorted_cores]
    y_ticks = [3, 4, 5, 6, 7, 8, 9]
    y_labels = [str(1<<i) for i in y_ticks]

    fig, ax = plt.subplots(1, 1, figsize=(7.5,5))
    graphs = [graph for graph in sv[cores]] # todo
    markers = [("o", "s"), ("v", "^")]
    for c in range(0, len(graphs)):
        graph = graphs[c]
        x_cc = []
        y_cc = []
        x_sv = []
        y_sv = []
        index = 0
        for cores in sorted_cores:
            if (graph in sv[cores]):
                x_sv.append(index)
                y_sv.append(math.log(sv[cores][graph][1], 2))
            if (graph in cc[cores]):
                x_cc.append(index)
                y_cc.append(math.log(cc[cores][graph][1], 2))
            index += 1
        g = short(graph)
        #ax.set_title(g, fontsize=18)
        ax.plot(x_cc, y_cc, label=g+" (LACC)", marker=markers[c][0], markersize=9, linewidth=3)
        ax.plot(x_sv, y_sv, label=g+" (FastSV)", marker=markers[c][1], markersize=9, linewidth=3)
        #ax.set_xlim(xmin=0.8, xmax=4.2)
        #$ax.set_ylim(ymin=min(low-0.3, y_ticks[0]), ymax=y_ticks[-1])
    ax.grid()
    ax.legend(loc="upper right", fontsize=15)
    ax.set_xticks(x_ticks)
    ax.set_xticklabels(x_labels, fontsize=15)
    ax.set_yticks(y_ticks)
    ax.set_yticklabels(y_labels, fontsize=15)
    ax.set_xlabel("Number of cores", fontsize=18);
    ax.set_ylabel("Time (sec)", fontsize=18);
    #fig.text(-0.035, 0.5, "Time in seconds (log scale)", fontsize=22, va="center", rotation="vertical")
    #fig.text(0.5, -0.035, "Number of cores (log scale)", fontsize=22, ha="center")
    fig.tight_layout()
    fig.savefig("large-graph.pdf", bbox_inches="tight")

#latex_description()
#iters()
#density()
#scalability()
large_graph()
#breakdown_sv()
#breakdown_cc()