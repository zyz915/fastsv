
/*
    LAGraph:  graph algorithms based on GraphBLAS

    Copyright 2019 LAGraph Contributors.

    (see Contributors.txt for a full list of Contributors; see
    ContributionInstructions.txt for information on how you can Contribute to
    this project).

    All Rights Reserved.

    NO WARRANTY. THIS MATERIAL IS FURNISHED ON AN "AS-IS" BASIS. THE LAGRAPH
    CONTRIBUTORS MAKE NO WARRANTIES OF ANY KIND, EITHER EXPRESSED OR IMPLIED,
    AS TO ANY MATTER INCLUDING, BUT NOT LIMITED TO, WARRANTY OF FITNESS FOR
    PURPOSE OR MERCHANTABILITY, EXCLUSIVITY, OR RESULTS OBTAINED FROM USE OF
    THE MATERIAL. THE CONTRIBUTORS DO NOT MAKE ANY WARRANTY OF ANY KIND WITH
    RESPECT TO FREEDOM FROM PATENT, TRADEMARK, OR COPYRIGHT INFRINGEMENT.

    Released under a BSD license, please see the LICENSE file distributed with
    this Software or contact permission@sei.cmu.edu for full terms.

    Created, in part, with funding and support from the United States
    Government.  (see Acknowledgments.txt file).

    This program includes and/or can make use of certain third party source
    code, object code, documentation and other files ("Third Party Software").
    See LICENSE file for more details.

*/

#define LAGRAPH_FREE_ALL

#include "LAGraph.h"

GrB_Info my_assign(GrB_Vector w, GrB_Vector inp, GrB_Index *I, GrB_Index n,
		GrB_Index *index, GrB_Index *value, GrB_Index *input)
{
	GrB_Vector_extractTuples(index, value, &n, w);
	GrB_Vector_extractTuples(index, input, &n, inp);
	for (GrB_Index i = 0; i < n; i++)
		if (input[i] < value[I[i]])
			value[I[i]] = input[i];
	GrB_Vector_clear(w);
	GrB_Vector_build(w, index, value, n, GrB_PLUS_UINT64);
	return GrB_SUCCESS;
}

GrB_Info LAGraph_fast_sv(GrB_Matrix A, GrB_Vector *result)
{
    GrB_Info info;
    GrB_Index n;
    GrB_Vector f = NULL, gp = NULL, dup = NULL, mngp = NULL;
    GrB_Vector mod = NULL, ans = NULL;
    GrB_Monoid Min = NULL, Add = NULL;
    GrB_Semiring sel2ndMin = NULL;

    LAGRAPH_OK (GrB_Matrix_nrows (&n, A)) ;
    LAGRAPH_OK (GrB_Vector_new (&f,   GrB_UINT64, n));
    LAGRAPH_OK (GrB_Vector_new (&dup, GrB_UINT64, n));
    LAGRAPH_OK (GrB_Vector_new (&mngp, GrB_UINT64, n));
    LAGRAPH_OK (GrB_Vector_new (&mod, GrB_UINT64, n));
    LAGRAPH_OK (GrB_Vector_new (&gp,  GrB_UINT64, n));
    LAGRAPH_OK (GrB_Vector_new (&ans, GrB_UINT64, n));
    GrB_Index *I = LAGraph_malloc(n, sizeof(GrB_Index));
    GrB_Index *V = LAGraph_malloc(n, sizeof(GrB_Index));
    for (GrB_Index i = 0; i < n; i++)
        I[i] = V[i] = i;
    LAGRAPH_OK (GrB_Vector_build (f, I, V, n, GrB_PLUS_UINT64));
    LAGRAPH_OK (GrB_Vector_dup(&gp,  f));
    LAGRAPH_OK (GrB_Vector_dup(&dup, f));
    LAGRAPH_OK (GrB_Vector_dup(&mngp,f));
    LAGRAPH_OK (GrB_Vector_dup(&ans, f));
    LAGRAPH_OK (GrB_Monoid_new(&Min, GrB_MIN_UINT64, n));
    LAGRAPH_OK (GrB_Semiring_new(&sel2ndMin, Min, GrB_SECOND_UINT64));
    LAGRAPH_OK (GrB_Monoid_new(&Add, GrB_PLUS_UINT64, (GrB_Index)0));
    GrB_Index diff = n, nCC, iters = 0;
	// for my_assign
	GrB_Index *index = malloc(sizeof(GrB_Index) * n);
	GrB_Index *value = malloc(sizeof(GrB_Index) * n);
	GrB_Index *input = malloc(sizeof(GrB_Index) * n);
    while (diff != 0) {
		iters += 1;
        // tree hooking
        LAGRAPH_OK (GrB_mxv(mngp, NULL, GrB_MIN_UINT64, sel2ndMin, A, gp, NULL));
        // LAGRAPH_OK (GrB_assign(f, NULL, GrB_MIN_UINT64, mngp, V, n, NULL));
		my_assign (f, mngp, V, n, index, value, input);
        LAGRAPH_OK (GrB_eWiseMult(f, NULL, NULL, GrB_MIN_UINT64, f, mngp, NULL));
        // shortcutting
        LAGRAPH_OK (GrB_eWiseMult(f, NULL, NULL, GrB_MIN_UINT64, f, gp, NULL));
        // calculate grandparent
        LAGRAPH_OK (GrB_Vector_extractTuples(I, V, &n, f));
        LAGRAPH_OK (GrB_extract(gp, NULL, NULL, f, V, n, NULL));
        // termination
        LAGRAPH_OK (GrB_eWiseMult(mod, NULL, NULL, GxB_ISNE_UINT64, dup, gp, NULL));
        LAGRAPH_OK (GrB_reduce(&diff, NULL, Add, mod, NULL));
        LAGRAPH_OK (GrB_Vector_dup(&dup, gp));
        // printf("iteration %lu: diff %lu\n", iters, diff);
    }
    LAGRAPH_OK (GrB_eWiseMult(mod, NULL, NULL, GxB_ISEQ_UINT64, ans, gp, NULL));
    LAGRAPH_OK (GrB_reduce(&nCC, NULL, Add, mod, GrB_NULL));
    printf("number of iterations: %lu\n", iters);
    printf("number of components: %lu\n", nCC);
    *result = f;
	// for my_assign
	free(index);
	free(value);
	free(input);

    GrB_free (&gp);
    GrB_free (&mngp);
    GrB_free (&dup);
    GrB_free (&mod);
    GrB_free (&ans);
    GrB_free (&Add);
    GrB_free (&Min);
    GrB_free (&sel2ndMin);

    return GrB_SUCCESS;
}


