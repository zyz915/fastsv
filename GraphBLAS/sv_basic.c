#define FREE_ALL                    \
{                                   \
    GrB_free (&A) ;                 \
    GrB_free (&f) ;                 \
    GrB_free (&gf) ;                \
    GrB_free (&mnp) ;               \
    GrB_free (&dup) ;               \
    GrB_free (&mod) ;               \
    GrB_free (&ans) ;               \
    GrB_free (&Add) ;               \
    GrB_free (&Min) ;               \
    GrB_free (&sel2ndMin);          \
}

#include "demos.h"

int main (int argc, char **argv)
{

    //--------------------------------------------------------------------------
    // initializations
    //--------------------------------------------------------------------------

    GrB_Info info ;
    GrB_Matrix A = NULL ;
    GrB_Vector f = NULL, dup = NULL, mnp = NULL, mask = NULL;
    GrB_Vector mod = NULL, gf = NULL, ans = NULL, v = NULL, h = NULL;
    GrB_Monoid Min = NULL, Add = NULL;
    GrB_Semiring sel2ndMin = NULL;

    //--------------------------------------------------------------------------
    // read a matrix from stdin
    //--------------------------------------------------------------------------

    bool one_based = false ;
    if (argc > 1) one_based = strtol (argv [1], NULL, 0) ;

    OK (read_matrix (&A,
        stdin,      // read matrix from stdin
        false,      // unsymmetric
        false,      // self edges OK
        one_based,  // 0-based or 1-based, depending on input arg
        true,       // read input file as Boolean
        false)) ;    // print status to stdout

    GrB_Index n, nvals, nhooks;
    OK (GrB_Matrix_nrows (&n, A)) ;
    OK (GrB_Matrix_nvals (&nvals, A)) ;

    printf("n = %llu, nvals = %llu\n", n, nvals);

    OK (GrB_Vector_new (&f,   GrB_UINT64, n));
    OK (GrB_Vector_new (&dup, GrB_UINT64, n));
    OK (GrB_Vector_new (&mnp, GrB_UINT64, n));
    OK (GrB_Vector_new (&mod, GrB_UINT64, n));
    OK (GrB_Vector_new (&gf,  GrB_UINT64, n));
    OK (GrB_Vector_new (&ans, GrB_UINT64, n));
    OK (GrB_Vector_new (&mask,  GrB_BOOL, n));
    OK (GrB_Vector_new (&h,   GrB_UINT64, n));
    GrB_Index *I = malloc(sizeof(GrB_Index) * n);
    GrB_Index *V = malloc(sizeof(GrB_Index) * n);
    for (int i = 0; i < n; i++)
        I[i] = V[i] = i;
    OK (GrB_Vector_build (f, I, V, n, GrB_PLUS_UINT64));
    OK (GrB_Vector_dup(&dup, f));
    //OK (GrB_Vector_dup(&mnp, f));
    OK (GrB_Vector_dup(&gf,  f));
    OK (GrB_Vector_dup(&ans, f));
    OK( GrB_Monoid_new(&Min, GrB_MIN_UINT64, n));
    OK( GrB_Semiring_new(&sel2ndMin, Min, GrB_SECOND_UINT64));
    OK( GrB_Monoid_new(&Add, GrB_PLUS_UINT64, (GrB_Index)0));
    GrB_Index diff = n, nCC;
    for (int iter = 1; diff != 0; iter++) {
        // calculate grandparent
        OK (GrB_Vector_extractTuples(I, V, &n, f));
        OK (GrB_extract(gf, NULL, NULL, f, V, n, NULL));
        OK (GrB_eWiseMult(mask, NULL, NULL, GxB_ISEQ_UINT64, gf, f, NULL))
        // tree hooking
        OK (GrB_mxv(mnp, mask, NULL, sel2ndMin, A, f, NULL));
        OK (GrB_eWiseMult(h, NULL, NULL, GrB_SECOND_UINT64, mnp, f, NULL));
        OK (GrB_Vector_nvals(&nhooks, h));
        OK (GrB_Vector_extractTuples(I, V, &nhooks, h));
        OK (GrB_Vector_new(&v, GrB_UINT64, nhooks));
        OK (GrB_extract(v, mnp, NULL, mnp, I, nhooks, NULL));
        OK (GrB_assign(f, NULL, GrB_MIN_UINT64, v, V, nhooks, NULL));
        // shortcutting
        // OK (GrB_Vector_extractTuples(I, V, &n, f));
        // OK (GrB_extract(f, NULL, NULL, f, V, n, NULL));
        OK (GrB_eWiseMult(f, NULL, NULL, GrB_MIN_UINT64, f, gf, NULL));
        // termination
        OK (GrB_eWiseMult(mod, NULL, NULL, GxB_ISNE_UINT64, dup, f, NULL));
        OK (GrB_reduce(&diff, NULL, Add, mod, NULL));
        OK (GrB_Vector_dup(&dup, f));
        printf("iteration %d: diff %llu\n", iter, diff);
        OK (GrB_Vector_clear(mnp));
        OK (GrB_Vector_clear(v));
    }
    // calc # of CCs
    OK (GrB_eWiseMult(mod, NULL, NULL, GxB_ISEQ_UINT64, ans, f, NULL));
    OK (GrB_reduce(&nCC, NULL, Add, mod, NULL));
    printf("number of CCs: %llu\n", nCC);

    FREE_ALL ;
    GrB_finalize ( ) ;
}

